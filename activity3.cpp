#include <iostream>

using namespace std;

void swap(int* a, int* b);
void reverse(char s[]);

int main(){

  char arr[]={'H','a','r','r','y','\0'};
  int arrSize = sizeof(arr)/sizeof(arr[0]);
  
  reverse(arr);

  for (int i = 0; i < arrSize; i++){
    cout << arr[i];
  }

}

void swap(int *firstVal, int *secondVal){ 
    int swapper = *firstVal; 
    *firstVal = *secondVal; 
    *secondVal = swapper; 
} 

void reverse(char s[]){

  int arrSize = sizeof(s)/sizeof(s[0]);

  char *point1 = s;
  char *point2 = s + arrSize - 1; 
    while (point1 < point2) { 
        swap(point1, point2); 
        point1++; 
        point2--; 
    } 
  
}
