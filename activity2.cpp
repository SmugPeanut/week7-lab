#include <iostream>

using namespace std;

int main(){

  double myDouble = 2.4;
  double *address = &myDouble;

  cout << "Variable val: " << myDouble<< endl ;
  cout << "Variable address: " << &myDouble << endl;

  cout << "Value of pointer: " << address << endl;
  cout << "Pointer address: " << &address << endl;
  cout << "Dereferencing the pointer: " << *address << endl;
}
